import cv2
import os

pathIn= 'y_res_256/'
pathOut = 'y_res_256.mp4'
fps = 15
frame_array = []

paths = os.listdir(pathIn)
'''
#tmp = []
for path in paths:
    
    print(path)
    path_num = path.replace('.jpg','')
    print(path_num)
    num = int(path_num)
    print(num)
    #path[0] = format(num,'05')
    print(format(num,'05')+".jpg")
    #tmp.append(format(num,'05')+".jpg")
    os.rename(os.path.join(os.getcwd(),pathIn,path) , os.path.join(os.getcwd(),pathIn,format(num,'05')+".jpg"))
    
#paths = tmp.copy()
'''
paths.sort()


for idx , path in enumerate(paths) : 
    print(path)
    img = cv2.imread(os.path.join(os.getcwd(),pathIn,path))
    height, width, layers = img.shape
    size = (width,height)
    frame_array.append(img)
out = cv2.VideoWriter(pathOut,cv2.VideoWriter_fourcc(*'mp4v'), fps, size)
for i in range(len(frame_array)):
    # writing to a image array
    out.write(frame_array[i])
out.release()

