"""
Inference on webcams: Use a model on webcam input.

Once launched, the script is in background collection mode.
Press B to toggle between background capture mode and matting mode. The frame shown when B is pressed is used as background for matting.
Press Q to exit.

Example:

    python inference_webcam.py \
        --model-type mattingrefine \
        --model-backbone resnet50 \
        --model-checkpoint "PATH_TO_CHECKPOINT" \
        --resolution 1280 720

"""

import argparse, os, shutil, time
import cv2
import torch

from torch import nn
from torch.utils.data import DataLoader
from torchvision.transforms import Compose, ToTensor, Resize
from torchvision.transforms.functional import to_pil_image
from threading import Thread, Lock
from tqdm import tqdm
from PIL import Image

from dataset import VideoDataset
from model import MattingBase, MattingRefine
import math

# --------------- Arguments ---------------


parser = argparse.ArgumentParser(description='Inference from web-cam')

parser.add_argument('--model-type', type=str, required=True, choices=['mattingbase', 'mattingrefine'])
parser.add_argument('--model-backbone', type=str, required=True, choices=['resnet101', 'resnet50', 'mobilenetv2'])
parser.add_argument('--model-backbone-scale', type=float, default=0.25)
parser.add_argument('--model-checkpoint', type=str, required=True)
parser.add_argument('--model-refine-mode', type=str, default='sampling', choices=['full', 'sampling', 'thresholding'])
parser.add_argument('--model-refine-sample-pixels', type=int, default=80_000)
parser.add_argument('--model-refine-threshold', type=float, default=0.7)

parser.add_argument('--hide-fps', action='store_true')
parser.add_argument('--resolution', type=int, nargs=2, metavar=('width', 'height'), default=(1280, 720))
args = parser.parse_args()

# An FPS tracker that computes exponentialy moving average FPS
class FPSTracker:
    def __init__(self, ratio=0.5):
        self._last_tick = None
        self._avg_fps = None
        self.ratio = ratio
    def tick(self):
        if self._last_tick is None:
            self._last_tick = time.time()
            return None
        t_new = time.time()
        fps_sample = 1.0 / (t_new - self._last_tick)
        self._avg_fps = self.ratio * fps_sample + (1 - self.ratio) * self._avg_fps if self._avg_fps is not None else fps_sample
        self._last_tick = t_new
        return self.get()
    def get(self):
        return self._avg_fps

# Wrapper for playing a stream with cv2.imshow(). It can accept an image and return keypress info for basic interactivity.
# It also tracks FPS and optionally overlays info onto the stream.
class Displayer:
    def __init__(self, title, width=None, height=None, show_info=True):
        self.title, self.width, self.height = title, width, height
        self.show_info = show_info
        self.fps_tracker = FPSTracker()
        cv2.namedWindow(self.title, cv2.WINDOW_NORMAL)
        if width is not None and height is not None:
            cv2.resizeWindow(self.title, width, height)
    # Update the currently showing frame and return key press char code
    def step(self, image):
        fps_estimate = self.fps_tracker.tick()
        if self.show_info and fps_estimate is not None:
            message = f"{int(fps_estimate)} fps | {self.width}x{self.height}"
            cv2.putText(image, message, (10, 40), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0, 0, 0))
        cv2.imshow(self.title, image)
        return cv2.waitKey(1) & 0xFF

# --------------- Main ---------------


# Load model
if args.model_type == 'mattingbase':
    model = MattingBase(args.model_backbone)
if args.model_type == 'mattingrefine':
    model = MattingRefine(
        args.model_backbone,
        args.model_backbone_scale,
        args.model_refine_mode,
        args.model_refine_sample_pixels,
        args.model_refine_threshold)

model = model.cuda().eval()
model.load_state_dict(torch.load(args.model_checkpoint), strict=False)


width, height = args.resolution
#cam = Camera(width=width, height=height)
#dsp = Displayer('MattingV2', cam.width, cam.height, show_info=(not args.hide_fps))

def cv2_frame_to_cuda(frame):
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    return ToTensor()(Image.fromarray(frame)).unsqueeze_(0).cuda()

import numpy as np

# ADD
from efficientdet.backbone import EfficientDetBackbone
from efficientdet.utils.utils import preprocess_imgae, postprocess, invert_affine, plot_one_box, get_index_label, standard_to_bgr,  STANDARD_COLORS
from efficientdet.efficientdet.utils import BBoxTransform, ClipBoxes

obj_list = ['person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', 'train', 'truck', 'boat', 'traffic light',
            'fire hydrant', '', 'stop sign', 'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep',
            'cow', 'elephant', 'bear', 'zebra', 'giraffe', '', 'backpack', 'umbrella', '', '', 'handbag', 'tie',
            'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball', 'kite', 'baseball bat', 'baseball glove',
            'skateboard', 'surfboard', 'tennis racket', 'bottle', '', 'wine glass', 'cup', 'fork', 'knife', 'spoon',
            'bowl', 'banana', 'apple', 'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza', 'donut',
            'cake', 'chair', 'couch', 'potted plant', 'bed', '', 'dining table', '', '', 'toilet', '', 'tv',
            'laptop', 'mouse', 'remote', 'keyboard', 'cell phone', 'microwave', 'oven', 'toaster', 'sink',
            'refrigerator', '', 'book', 'clock', 'vase', 'scissors', 'teddy bear', 'hair drier',
            'toothbrush']
color_list = standard_to_bgr(STANDARD_COLORS)
def display(preds, imgs, imshow=True, imwrite=False):
    print(len(imgs))
    background_image = np.ones(imgs[0].shape)
    tmp_x1, tmp_y1, tmp_x2, tmp_y2 = -1, -1, -1, -1
    for i in range(len(imgs)):
        if len(preds[i]['rois']) == 0:
            continue
        #background_image = np.ones(imgs[i].shape)
        imgs[i] = imgs[i].copy()

        for j in range(len(preds[i]['rois'])):
            obj = obj_list[preds[i]['class_ids'][j]]
            if obj == 'person':
                x1, y1, x2, y2 = preds[i]['rois'][j].astype(np.int)
                # test 4
                tmp_x1, tmp_y1, tmp_x2, tmp_y2 = x1, y1, x2, y2
                if (y2 - y1) / 10 > y1:
                    tmp_y1 = 0
                else:
                    tmp_y1 = y1 - int((y2 - y1) / 10)
                if y2 + (y2 - y1) * 0.3 >  imgs[i].shape[0]:
                    tmp_y2 =  imgs[i].shape[0]
                else:
                    tmp_y2 = y2 + int((y2 - y1) / 0.3)
                if (x2 - x1) / 10 > x1:
                    tmp_x1 = 0
                else:
                    tmp_x1 = x1 - int((x2 - x1) / 10)
                if x2 + (x2 - x1) / 10 > imgs[i].shape[1]:
                    tmp_x2 = imgs[i].shape[1]
                else:
                    tmp_x2 = x2 + int((x2 - x1) / 10)

                score = float(preds[i]['scores'][j])
                #plot_one_box(imgs[i], [x1, y1, x2, y2], label=obj,score=score,color=color_list[get_index_label(obj, obj_list)])
                #background_image = cv2.rectangle(background_image, (x1, y1), (x2, y2), (0,0,0), -1)
                # test 4
                plot_one_box(imgs[i], [tmp_x1, tmp_y1, tmp_x2, tmp_y2 ], label=obj, score=score,
                             color=color_list[get_index_label(obj, obj_list)])
                background_image = cv2.rectangle(background_image, (tmp_x1, tmp_y1), (tmp_x2, tmp_y2), (0,0,0), -1)


        # if imshow:
        #     cv2.imshow('background_image', cv2.resize(background_image, dsize=(0, 0), fx=0.2, fy=0.2, interpolation=cv2.INTER_LINEAR))
        #
        #     cv2.imshow('img', cv2.resize(imgs[i], dsize=(0, 0), fx=0.2, fy=0.2, interpolation=cv2.INTER_LINEAR))
        #     cv2.waitKey(1)
        #
        # if imwrite:
        #     cv2.imwrite(f'test/img_inferred_d{compound_coef}_this_repo_{i}.jpg', imgs[i])

    return background_image, tmp_x1, tmp_y1, tmp_x2, tmp_y2



use_cuda = True
use_float16 = False
threshold = 0.2
iou_threshold = 0.2
compound_coef = 0
force_input_size = None  # set None to use default size
anchor_ratios = [(1.0, 1.0), (1.4, 0.7), (0.7, 1.4)]
anchor_scales = [2 ** 0, 2 ** (1.0 / 3.0), 2 ** (2.0 / 3.0)]


efficientDet_input_sizes = [512, 640, 768, 896, 1024, 1280, 1280, 1536, 1536]
input_size = efficientDet_input_sizes[compound_coef] if force_input_size is None else force_input_size

EfficientDet_model = EfficientDetBackbone(compound_coef=compound_coef, num_classes=len(obj_list),
                             ratios=anchor_ratios, scales=anchor_scales)
EfficientDet_model.load_state_dict(torch.load(f'efficientdet/weights/efficientdet-d{compound_coef}.pth', map_location='cpu'))
EfficientDet_model.requires_grad_(False)
EfficientDet_model.eval()

if use_cuda:
    EfficientDet_model = EfficientDet_model.cuda()
if use_float16:
    EfficientDet_model = EfficientDet_model.half()

#fourcc = cv2.VideoWriter_fourcc(*'DIVX') # 코덱 정의
#out = cv2.VideoWriter('otter_out.avi', fourcc, 30, (int(width), int(height))) # VideoWriter 객체 정의

with torch.no_grad():

    regressBoxes = BBoxTransform()
    clipBoxes = ClipBoxes()

    cap = cv2.VideoCapture('vid/6.mp4')
    ret, frame = cap.read()

    frameNUmber = 1
    frame_bgr = None

    while True:
        bgr = None
        src = None
        ret, frame = cap.read()

        if frameNUmber == 1:
            frame_bgr = frame
            #test 2
            avg = np.float32(frame)


        #frame = cam.read()
        t1 = time.time()
        ori_imgs, framed_imgs, framed_metas = preprocess_imgae(frame, max_size=input_size)

        if use_cuda:
            x = torch.stack([torch.from_numpy(fi).cuda() for fi in framed_imgs], 0)
        else:
            x = torch.stack([torch.from_numpy(fi) for fi in framed_imgs], 0)

        x = x.to(torch.float32 if not use_float16 else torch.float16).permute(0, 3, 1, 2)


        features, regression, classification, anchors = EfficientDet_model(x)

        out = postprocess(x,
                          anchors, regression, classification,
                          regressBoxes, clipBoxes,
                          threshold, iou_threshold)
        out = invert_affine(framed_metas, out)
        t2 = time.time()
        tact_time = (t2 - t1) / 1
        print(f'{tact_time} seconds, {1 / tact_time} FPS, @batch_size 1')
        backimg, tmp_x1, tmp_y1, tmp_x2, tmp_y2 = display(out, ori_imgs, imshow=True, imwrite=False)
        frame_bgr = frame_bgr * (1- backimg) + (backimg) * frame
        frame_bgr = frame_bgr.astype(np.uint8)
        print("out:", tmp_x1, tmp_y1, tmp_x2, tmp_y2)
        if tmp_x1 != -1 and tmp_y1 != -1 and tmp_x2 != -1 and tmp_y2 != -1:
            if (tmp_y2 - tmp_y1) > (tmp_x2 - tmp_x1):
                #print("Crop Y")
                #print("X : " + str(tmp_x2 - tmp_x1) + " Y: " + str(tmp_y2 - tmp_y1))
                #distnce = (math.ceil((tmp_y2 - tmp_y1) / 4)) * 4
                centerY = tmp_y1 + (tmp_y2 - tmp_y1) / 2.0
                centerX = tmp_x1 + (tmp_x2 - tmp_x1) / 2.0
                cropY1 = int(centerY - ((tmp_y2 - tmp_y1)/2.0))
                cropY2 = int(centerY + (tmp_y2 - tmp_y1) / 2.0)
                cropX1 = int(centerX - (tmp_x2 - tmp_x1) / 2.0)
                cropX2 = int(centerX + (tmp_x2 - tmp_x1) / 2.0)
                print(centerX, centerY, cropX1, cropX2, cropY1, cropY2)
            else:
                #print("Crop X")
                #print("X : " + str(tmp_x2 - tmp_x1) + " Y: " + str(tmp_y2 - tmp_y1))
                #distnce = (math.ceil((tmp_y2 - tmp_y1) / 4)) * 4
                centerY = tmp_y1 + (tmp_y2 - tmp_y1) / 2.0
                centerX = tmp_x1 + (tmp_x2 - tmp_x1) / 2.0
                cropY1 = int(centerY - ((tmp_y2 - tmp_y1) / 2.0))
                cropY2 = int(centerY + (tmp_y2 - tmp_y1) / 2.0)
                cropX1 = int(centerX - (tmp_x2 - tmp_x1) / 2.0)
                cropX2 = int(centerX + (tmp_x2 - tmp_x1) / 2.0)
                print(centerX, centerY, cropX1, cropX2, cropY1, cropY2)

            distanceY = (math.ceil((cropY2 - cropY1) / 4)) * 4
            distanceX = (math.ceil((cropX2 - cropX1) / 4)) * 4

            if (cropY2 - cropY1) % 2 == 1:
                cropY2 += 1

            if (cropX2 - cropX1) % 2 == 1:
                cropX2 += 1

            diff_distance_X = distanceY - (cropX2 - cropX1)
            diff_distance_Y = distanceY - (cropY2 - cropY1)



            #print(distanceY, diff_distance_X, diff_distance_Y, cropX1, cropX2, cropY1, cropY2)
            #
            # if diff_distance_X%2 == 1:
            #     diff_distance_X += 1
            #
            # if diff_distance_Y%2 == 1:
            #     diff_distance_Y += 1

            temp_value_x = diff_distance_X / 2.0
            temp_value_y = diff_distance_Y / 2.0

            print(temp_value_x, temp_value_y)

            cropX1 = cropX1 - temp_value_x
            cropX2 = cropX2 + temp_value_x

            cropY1 = cropY1 - temp_value_y
            cropY2 = cropY2 + temp_value_y
            print("first", centerX, centerY, cropX1, cropX2, cropY1, cropY2)

            if cropX1 < 0:
                cropX2 += cropX1*(-1)
                cropX1 = 0
                if cropX2 > frame.shape[1]:
                    cropX2 = frame.shape[1]

            elif cropX2 > frame.shape[1]:
                cropX1 -= (cropX2 - frame.shape[1])
                cropX2 = frame.shape[1]
                if cropX1 < 0:
                    cropX1 = 0

            if cropX1 < 0 and cropX2 > frame.shape[1]:
                cropX1 = 0
                cropX2 = frame.shape[1]


            if cropY1 < 0:
                cropY2 += cropY1 * (-1)
                cropY1 = 0
                if cropY2 > frame.shape[0]:
                    cropY2 = frame.shape[0]

            elif cropY2 > frame.shape[0]:
                cropY1 -= (cropY2 - frame.shape[0])
                cropY2 = frame.shape[0]
                if cropY1 < 0:
                    cropY1 = 0

            if cropY1 < 0 and cropY2 > frame.shape[0]:
                cropY1 = 0
                cropY2 = frame.shape[1]
            cropX1, cropX2, cropY1, cropY2 = int(cropX1), int(cropX2), int(cropY1), int(cropY2)
            print("Difine : ", cropX1, cropX2, cropY1, cropY2)
            #frame_bgr = frame_bgr[cropY1:cropY2, cropX1:cropX2]
            print("Crop Size : " + str(cropY2 - cropY1) + " "  + str(cropX2 - cropX1))
            if cropY2 - cropY1 > 256 and cropX2 - cropX1 > 256:
                bgr = cv2_frame_to_cuda(frame_bgr[cropY1:cropY2, cropX1:cropX2])
                src = cv2_frame_to_cuda(frame[cropY1:cropY2, cropX1:cropX2])

                t1 = time.time()
                pha, fgr = model(src, bgr)[:2]
                res = pha * fgr + (1 - pha) * torch.ones_like(fgr)

                res = res.mul(255).byte().cpu().permute(0, 2, 3, 1).numpy()[0]
                res = cv2.cvtColor(res, cv2.COLOR_RGB2BGR)

                up_res = np.ones(frame.shape, dtype = "uint8")*255
                up_res[cropY1:cropY2, cropX1:cropX2] =res
                res = up_res
                t2 = time.time()
                tact_time = (t2 - t1) / 1
                print(f'{tact_time} seconds, {1 / tact_time} FPS, @batch_size 1')
                # key = dsp.step(res)
                cv2.imshow("Result1 ",
                           cv2.resize(backimg, dsize=(0, 0), fx=0.2, fy=0.2, interpolation=cv2.INTER_LINEAR))
                cv2.imshow("bgr ", cv2.resize(frame_bgr, dsize=(0, 0), fx=0.2, fy=0.2, interpolation=cv2.INTER_LINEAR))

                cv2.imshow("Re", cv2.resize(res, dsize=(0, 0), fx=0.2, fy=0.2, interpolation=cv2.INTER_LINEAR))
                cv2.imwrite('test7/bg/' + format(frameNUmber, '04') + ".jpg", frame_bgr)
                cv2.imwrite('test7/matting/' + format(frameNUmber, '04') + ".jpg", res)
                cv2.imwrite('test7/sg/' + format(frameNUmber, '04') + ".jpg", backimg)

            else:
                bgr = cv2_frame_to_cuda(frame_bgr)
                src = cv2_frame_to_cuda(frame)

                t1 = time.time()
                pha, fgr = model(src, bgr)[:2]
                res = pha * fgr + (1 - pha) * torch.ones_like(fgr)
                res = res.mul(255).byte().cpu().permute(0, 2, 3, 1).numpy()[0]
                res = cv2.cvtColor(res, cv2.COLOR_RGB2BGR)
                t2 = time.time()
                tact_time = (t2 - t1) / 1
                print(f'{tact_time} seconds, {1 / tact_time} FPS, @batch_size 1')
                # key = dsp.step(res)
                cv2.imshow("Result1 ",
                           cv2.resize(backimg, dsize=(0, 0), fx=0.2, fy=0.2, interpolation=cv2.INTER_LINEAR))
                cv2.imshow("bgr ", cv2.resize(frame_bgr, dsize=(0, 0), fx=0.2, fy=0.2, interpolation=cv2.INTER_LINEAR))

                cv2.imshow("Re", cv2.resize(res, dsize=(0, 0), fx=0.2, fy=0.2, interpolation=cv2.INTER_LINEAR))
                cv2.imwrite('test7/bg/' + format(frameNUmber, '04') + ".jpg", frame_bgr)
                cv2.imwrite('test7/matting/' + format(frameNUmber, '04') + ".jpg", res)
                cv2.imwrite('test7/sg/' + format(frameNUmber, '04') + ".jpg", backimg)

            cv2.imwrite('test7/cur/' + format(frameNUmber, '04') + ".jpg", frame[cropY1:cropY2, cropX1:cropX2])
        else:
            bgr = cv2_frame_to_cuda(frame_bgr)
            src = cv2_frame_to_cuda(frame)

            t1 = time.time()
            pha, fgr = model(src, bgr)[:2]
            res = pha * fgr + (1 - pha) * torch.ones_like(fgr)
            res = res.mul(255).byte().cpu().permute(0, 2, 3, 1).numpy()[0]
            res = cv2.cvtColor(res, cv2.COLOR_RGB2BGR)
            t2 = time.time()
            tact_time = (t2 - t1) / 1
            print(f'{tact_time} seconds, {1 / tact_time} FPS, @batch_size 1')
            # key = dsp.step(res)
            cv2.imshow("Result1 ", cv2.resize(backimg, dsize=(0, 0), fx=0.2, fy=0.2, interpolation=cv2.INTER_LINEAR))
            cv2.imshow("bgr ", cv2.resize(frame_bgr, dsize=(0, 0), fx=0.2, fy=0.2, interpolation=cv2.INTER_LINEAR))

            cv2.imshow("Re", cv2.resize(res, dsize=(0, 0), fx=0.2, fy=0.2, interpolation=cv2.INTER_LINEAR))
            cv2.imwrite('test7/bg/' + format(frameNUmber, '04') + ".jpg", frame_bgr)
            cv2.imwrite('test7/matting/' + format(frameNUmber, '04') + ".jpg", res)
            cv2.imwrite('test7/sg/' + format(frameNUmber, '04') + ".jpg", backimg)

        # test 2
        # cv2.accumulateWeighted(frame_bgr, avg, 0.005)
        # result = cv2.convertScaleAbs(avg)
        # #
        # bgr = cv2_frame_to_cuda(result)

        # t1 = time.time()
        # pha, fgr = model(src, bgr)[:2]
        # res = pha * fgr + (1 - pha) * torch.ones_like(fgr)
        # res = res.mul(255).byte().cpu().permute(0, 2, 3, 1).numpy()[0]
        # res = cv2.cvtColor(res, cv2.COLOR_RGB2BGR)
        # t2 = time.time()
        # tact_time = (t2 - t1) / 1
        # print(f'{tact_time} seconds, {1 / tact_time} FPS, @batch_size 1')
        # #key = dsp.step(res)
        # cv2.imshow("Result1 ", cv2.resize(backimg, dsize=(0, 0), fx=0.2, fy=0.2, interpolation=cv2.INTER_LINEAR))
        # cv2.imshow("bgr ", cv2.resize(frame_bgr, dsize=(0, 0), fx=0.2, fy=0.2, interpolation=cv2.INTER_LINEAR))
        #
        # cv2.imshow("Re", cv2.resize(res, dsize=(0, 0), fx=0.2, fy=0.2, interpolation=cv2.INTER_LINEAR))
        # cv2.imwrite('test6/bg/' + format(frameNUmber, '04') + ".jpg", frame_bgr)
        # cv2.imwrite('test6/matting/' + format(frameNUmber, '04') + ".jpg", res)
        # cv2.imwrite('test6/sg/' + format(frameNUmber, '04') + ".jpg", backimg)
        #out.write(res)
        frameNUmber += 1
        if cv2.waitKey(1) & 0xFF == ord('q'):
            exit()
#out.release()
