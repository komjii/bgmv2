"""
Inference on webcams: Use a model on webcam input.
Once launched, the script is in background collection mode.
Press B to toggle between background capture mode and matting mode. The frame shown when B is pressed is used as background for matting.
Press Q to exit.
Example:
    python inference_asyn.py \
        --model-type mattingrefine \
        --model-backbone resnet50 \
        --model-checkpoint pytorch_resnet50.pth \
        --resolution 1280 720
"""

import argparse, os, shutil, time, sys
import cv2
import torch

from torch import nn
from torch.utils.data import DataLoader
from torchvision.transforms import Compose, ToTensor, Resize, Normalize
from torchvision.transforms.functional import to_pil_image
from threading import Thread, Lock
from tqdm import tqdm
from PIL import Image

from dataset import VideoDataset
from model import MattingBase, MattingRefine
import torch.backends.cudnn as cudnn
from yolact.yolact import Yolact
from yolact.utils.augmentations import BaseTransform, FastBaseTransform, Resize
import numpy as np
from yolact.data import cfg, set_cfg
from yolact.layers.output_utils import postprocess
from yolact.data import COLORS
from collections import defaultdict

from modnet.src.models.modnet import MODNet
import torch.nn.functional as F



# --------------- Arguments ---------------


parser = argparse.ArgumentParser(description='Inference from web-cam')

parser.add_argument('--model-type', type=str, required=True, choices=['mattingbase', 'mattingrefine'])
parser.add_argument('--model-backbone', type=str, required=True, choices=['resnet101', 'resnet50', 'mobilenetv2'])
parser.add_argument('--model-backbone-scale', type=float, default=0.25)
parser.add_argument('--model-checkpoint', type=str, required=True)
parser.add_argument('--model-refine-mode', type=str, default='sampling', choices=['full', 'sampling', 'thresholding'])
parser.add_argument('--model-refine-sample-pixels', type=int, default=80_000)
parser.add_argument('--model-refine-threshold', type=float, default=0.7)

parser.add_argument('--hide-fps', action='store_true')
parser.add_argument('--resolution', type=int, nargs=2, metavar=('width', 'height'), default=(1280, 720))
args = parser.parse_args()


# ----------- Utility classes -------------


# A wrapper that reads data from cv2.VideoCapture in its own thread to optimize.
# Use .read() in a tight loop to get the newest frame
class Camera:
    def __init__(self, device_id=0, width=1280, height=720):
        self.capture = cv2.VideoCapture(device_id)
        self.capture.set(cv2.CAP_PROP_FRAME_WIDTH, width)
        self.capture.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
        self.width = int(self.capture.get(cv2.CAP_PROP_FRAME_WIDTH))
        self.height = int(self.capture.get(cv2.CAP_PROP_FRAME_HEIGHT))
        # self.capture.set(cv2.CAP_PROP_BUFFERSIZE, 2)
        self.success_reading, self.frame = self.capture.read()
        self.read_lock = Lock()
        self.thread = Thread(target=self.__update, args=())
        self.thread.daemon = True
        self.thread.start()

    def __update(self):
        while self.success_reading:
            grabbed, frame = self.capture.read()
            with self.read_lock:
                self.success_reading = grabbed
                self.frame = frame

    def read(self):
        with self.read_lock:
            frame = self.frame.copy()
        return frame

    def __exit__(self, exec_type, exc_value, traceback):
        self.capture.release()

# An FPS tracker that computes exponentialy moving average FPS
class FPSTracker:
    def __init__(self, ratio=0.5):
        self._last_tick = None
        self._avg_fps = None
        self.ratio = ratio
    def tick(self):
        if self._last_tick is None:
            self._last_tick = time.time()
            return None
        t_new = time.time()
        fps_sample = 1.0 / (t_new - self._last_tick)
        self._avg_fps = self.ratio * fps_sample + (1 - self.ratio) * self._avg_fps if self._avg_fps is not None else fps_sample
        self._last_tick = t_new
        return self.get()
    def get(self):
        return self._avg_fps

# Wrapper for playing a stream with cv2.imshow(). It can accept an image and return keypress info for basic interactivity.
# It also tracks FPS and optionally overlays info onto the stream.
class Displayer:
    def __init__(self, title, width=None, height=None, show_info=True):
        self.title, self.width, self.height = title, width, height
        self.show_info = show_info
        self.cnt = 0
        self.fps_tracker = FPSTracker()
        cv2.namedWindow(self.title, cv2.WINDOW_NORMAL)
        if width is not None and height is not None:
            cv2.resizeWindow(self.title, width, height)
    # Update the currently showing frame and return key press char code
    def step(self, image, path = None):
        if path is not None:
            cv2.imwrite(os.path.join(path,str(self.cnt)+".jpg"),image)
        fps_estimate = self.fps_tracker.tick()
        if self.show_info and fps_estimate is not None:
            message = f"{int(fps_estimate)} fps | {self.width}x{self.height}"
            cv2.putText(image, message, (10, 40), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0, 0, 0))
        cv2.imshow(self.title, image)
        self.cnt = self.cnt + 1
        return cv2.waitKey(1) & 0xFF


# --------------- Main ---------------

# A wrapper that reads data from cv2.VideoCapture in its own thread to optimize.
# Use .read() in a tight loop to get the newest frame
class BGMv2:
    def __init__(self, cam, segmodel, model_type='mattingrefine', model_backbone = 'resnet50', model_backbone_scale = 0.25,  model_refine_mode ='sampling', model_refine_sample_pixels =80000, model_refine_threshold = 0.7):
        # Load model
        if model_type == 'mattingbase':
            self.model = MattingBase(model_backbone)
        if model_type == 'mattingrefine':
            self.model = MattingRefine(
                model_backbone,
                model_backbone_scale,
                model_refine_mode,
                model_refine_sample_pixels,
                model_refine_threshold)

        self.model = self.model.cuda().eval()
        self.model.load_state_dict(torch.load(args.model_checkpoint), strict=False)
        self.res = None
        self.bg = None

        self.cam = cam
        self.segmodel = segmodel

        self.read_lock = Lock()
        self.thread = Thread(target=self.__update, args=())
        self.thread.daemon = True
        self.thread.start()


    def __update(self):
        with torch.no_grad():
            while True:
                bst = time.time()
                self.bg = self.segmodel.read()

                if self.bg is not None:
                    print(self.bg.shape)

                    frame = self.cam.read()

                    bgm_to_cuda_t = time.time()
                    src = self.__cv2_frame_to_cuda(frame)
                    bgr = self.__cv2_frame_to_cuda(self.bg)
                    print("bgmv2 to cuda time : "+str(time.time()-bgm_to_cuda_t))

                    bst_inf_time = time.time()
                    pha, fgr = self.model(src, bgr)[:2]
                    print("bgmv2 inf time : "+str(time.time()-bst_inf_time))
                    
                    bgm_post_t = time.time()
                    res = pha * fgr + (1 - pha) * torch.ones_like(fgr)
                    res = res.mul(255).byte().cpu().permute(0, 2, 3, 1).numpy()[0]
                    self.res = cv2.cvtColor(res, cv2.COLOR_RGB2BGR)
                    print("bgmv2 post process time : "+str(time.time()-bgm_post_t))

                print("bgmv2 update time : "+str(time.time()-bst))


    def read(self):
        with self.read_lock:
            if self.res is None:
                return None
            frame = self.res.copy()
            return frame


    def __cv2_frame_to_cuda(self, frame):
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        return ToTensor()(Image.fromarray(frame)).unsqueeze_(0).cuda()

class MODNetMatte:
    def __init__(self, cam):
        self.net = MODNet(backbone_pretrained=False)
        self.net = nn.DataParallel(self.net).cuda()
        self.net.load_state_dict(torch.load("modnet/pretrained/modnet_webcam_portrait_matting.ckpt"))
        self.net.eval()
        self.ref_size = 512
        self.im_transform = Compose(
            [
                ToTensor(),
                Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
            ]
        )
        self.kernel = np.ones((3,3), np.uint8)
        self.img_numpy = None
        self.cam = cam
        self.cur_frame = None
        self.first = False
        self.prev_frame = None
        self.read_lock = Lock()
        self.thread = Thread(target=self.__update, args=())
        self.thread.daemon = True
        self.thread.start()
        
    def __update(self):
        with torch.no_grad():
            while True:
                mst = time.time()
                frame = self.cam.read()
                frame = cv2.resize(frame, dsize=(512, 512), interpolation=cv2.INTER_NEAREST)
                self.cur_frame = frame.copy()
                
                if self.first is False:
                    self.prev_frame = frame.copy()                    
                    self.first = True
                
                im = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                
                # unify image channels to 3
                if len(im.shape) == 2:
                    im = im[:, :, None]
                if im.shape[2] == 1:
                    im = np.repeat(im, 3, axis=2)
                elif im.shape[2] == 4:
                    im = im[:, :, 0:3]
                
                # convert image to PyTorch tensor
                im = Image.fromarray(im)
                im = self.im_transform(im)
                # add mini-batch dim
                im = im[None, :, :, :]
                # resize image for input
                im_b, im_c, im_h, im_w = im.shape
                if max(im_h, im_w) < self.ref_size or min(im_h, im_w) > self.ref_size:
                    if im_w >= im_h:
                        im_rh = self.ref_size
                        im_rw = int(im_w / im_h * self.ref_size)
                    elif im_w < im_h:
                        im_rw = self.ref_size
                        im_rh = int(im_h / im_w * self.ref_size)
                else:
                    im_rh = im_h
                    im_rw = im_w
                im_rw = im_rw - im_rw % 32
                im_rh = im_rh - im_rh % 32
                im = F.interpolate(im, size=(im_rh, im_rw), mode='area')
                # inference
                mod_inf_st = time.time()
                _, _, matte = self.net(im.cuda(), True)
                print("modnet inf time : "+str(time.time()-mod_inf_st))
                matte = matte.repeat(1, 3, 1, 1)
                matte = F.interpolate(matte, size=(im_h, im_w), mode='area')
                matte = matte[0].data.cpu().numpy().transpose(1, 2, 0)
                #self.img_numpy = matte.copy()
                #self.img_numpy = self.__prep_display(dets_out = preds, img=frame, h= None,w= None, undo_transform=False, class_color = False, mask_alpha=1.0)
                matte = cv2.resize(matte,dsize=(256,256),interpolation=cv2.INTER_NEAREST)
                matte[matte > 0.5] = 1
                modnet_dilate_st = time.time()
                matte = cv2.dilate(matte, self.kernel, iterations = 1).copy()
                print("modnet dilate time : "+str(time.time()-modnet_dilate_st))
                matte = matte.copy()
                h = matte.shape[0]
                w = matte.shape[1]
                matte = cv2.cvtColor(matte, cv2.COLOR_RGB2GRAY)
                #print(matte.shape)
                
                cur = self.cur_frame.copy()
                cur = cv2.resize(cur,dsize=(256,256),interpolation=cv2.INTER_NEAREST)#self.cur_frame = cv2.resize(self.cur_frame, dsize=(256, 256), interpolation=cv2.INTER_AREA)
                pre = self.prev_frame.copy()
                pre = cv2.resize(pre,dsize=(256,256),interpolation=cv2.INTER_NEAREST)
                bg = np.zeros(cur.shape, dtype=np.uint8)
                
                for y in range(0, h):
                    for x in range(0, w):
                        # threshold the pixel
                        # print(matte[y, x])
                        if matte[y, x] < 0.5:
                            bg[y, x] = cur[y, x]
                        else:
                            bg[y, x] = pre[y, x]
                
                self.prev_frame = bg.copy()
                bg = cv2.resize(bg, dsize=(1280, 720), interpolation=cv2.INTER_NEAREST)
                self.img_numpy = bg.copy()

                print("modnet update time : "+str(time.time()-mst))
    def read(self):
        with self.read_lock:
            if self.img_numpy is None:
                return None
            frame = self.img_numpy.copy()
            return frame

class YolactNet:
    def __init__(self, cam):
        cudnn.fastest = True
        torch.set_default_tensor_type('torch.cuda.FloatTensor')
        cfg.mask_proto_debug = 'store_true'

        self.net = Yolact()
        self.net.load_weights('yolact/weights/yolact_im700_54_800000.pth')
        self.net.eval()
        self.net = self.net.cuda()
        self.img_numpy = None
        self.cam = cam
        self.cur_frame = None

        self.first = False
        self.prev_frame = None
        self.read_lock = Lock()
        self.thread = Thread(target=self.__update, args=())
        self.thread.daemon = True
        self.thread.start()
        self.color_cache = defaultdict(lambda: {})

        self.kernel = np.ones((3,3), np.uint8)

    def __update(self):

        with torch.no_grad():
            while True:
                yst = time.time()
                frame = self.cam.read()
                self.cur_frame = frame
                if self.first is False:
                    self.prev_frame = frame.copy()
                    self.first = True

                frame = cv2.resize(frame, dsize=(500, 500), interpolation=cv2.INTER_AREA)

                frame = torch.from_numpy(frame).cuda().float()

                batch = FastBaseTransform()(frame.unsqueeze(0))
                preds = self.net(batch)
                seg_frame = self.__prep_display(dets_out = preds, img=frame, h= None,w= None, undo_transform=False, class_color = False, mask_alpha=1.0)
                
                seg_frame = cv2.resize(seg_frame,dsize=(256,256),interpolation=cv2.INTER_NEAREST)
                seg_frame = seg_frame.copy().astype('uint8')
                #seg_frame[seg_frame > 0.5] = 1
                yolact_dilate_st = time.time()
                seg_frame = cv2.dilate(seg_frame, self.kernel, iterations = 2)
                print("yolact dilate time : "+str(time.time()-yolact_dilate_st))
                h = seg_frame.shape[0]
                w = seg_frame.shape[1]
                
                cur = self.cur_frame.copy()
                cur = cv2.resize(cur,dsize=(256,256),interpolation=cv2.INTER_NEAREST)
                pre = self.prev_frame.copy()
                pre = cv2.resize(pre,dsize=(256,256),interpolation=cv2.INTER_NEAREST)
                bg = np.zeros(cur.shape, dtype=np.uint8)
                for y in range(0, h):
                    for x in range(0, w):
                        # threshold the pixel
                        #print(seg_frame[y, x])
                        if seg_frame[y, x][0]  <0.5:
                            bg[y, x] = cur[y, x]
                        else:
                            bg[y, x] = pre[y, x]
                
                self.prev_frame = bg.copy()
                bg = cv2.resize(bg, dsize=(1280, 720), interpolation=cv2.INTER_NEAREST)
                self.img_numpy = bg.copy()              
                print("yolact update time : "+str(time.time()-yst))

    def read(self):
        with self.read_lock:
            
            if self.img_numpy is None:
                return None
            frame = self.img_numpy.copy()
            return frame

    def __prep_display(self, dets_out, img, h, w, undo_transform=True, class_color=False, mask_alpha=1.0):
        """
        Note: If undo_transform=False then im_h and im_w are allowed to be None.
        """

        img_gpu = img / 255.0
        h, w, _ = img.shape

        save = cfg.rescore_bbox
        cfg.rescore_bbox = True
        t = postprocess(dets_out, w, h, visualize_lincomb=False,
                        crop_masks=True,
                        score_threshold=0.08)
        cfg.rescore_bbox = save

        iter = 0
        res = []
        tmp_l = []
        aaa = t[0].cpu().numpy()
        for a in aaa:
            if a == 0:
                tmp_l.append(iter)
            iter += 1
        idx = t[1].argsort(0, descending=True)[:len(aaa) - 1]

        for a in tmp_l:
            for id in idx:
                if a == id:
                    res.append(a)
        #print(res)
        if cfg.eval_mask_branch:
            # Masks are drawn on the GPU, so don't copy
            masks = t[3][res]
        classes, scores, boxes = [x[res].cpu().numpy() for x in t[:3]]

        num_dets_to_consider = min(10, classes.shape[0])
        for j in range(num_dets_to_consider):
            if scores[j] <0.08:
                num_dets_to_consider = j
                break

        def get_color(j, on_gpu=None):
            # print(classes)
            color_idx = (classes[j] * 5 if class_color else j * 5) % len(COLORS)
            # print(color_idx)
            if on_gpu is not None and color_idx in self.color_cache[on_gpu]:
                #print("hello")
                return self.color_cache[on_gpu][color_idx]
            else:
                color = COLORS[color_idx]
                if not undo_transform:
                    # The image might come in as RGB or BRG, depending
                    color = (color[2], color[1], color[0])
                    color = (0, 0, 0)
                if on_gpu is not None:
                    color = (0, 0, 0)
                    color = torch.Tensor(color).to(on_gpu).float() / 255.
                    self.color_cache[on_gpu][color_idx] = color
                return color

        # First, draw the masks on the GPU where we can do it really fast
        # Beware: very fast but possibly unintelligible mask-drawing code ahead
        # I wish I had access to OpenGL or Vulkan but alas, I guess Pytorch tensor operations will have to suffice
        if True and cfg.eval_mask_branch and num_dets_to_consider > 0:
            # After this, mask is of size [num_dets, h, w, 1]
            masks = masks[:num_dets_to_consider, :, :, None]

            # Prepare the RGB images for each mask given their color (size [num_dets, h, w, 1])
            colors = torch.cat(
                [get_color(j, on_gpu=img_gpu.device.index).view(1, 1, 1, 3) for j in range(num_dets_to_consider)],
                dim=0)
            masks_color = masks.repeat(1, 1, 1, 3) * colors * mask_alpha

            # This is 1 everywhere except for 1-mask_alpha where the mask is
            inv_alph_masks = masks * (-mask_alpha) + 1

            # I did the math for this on pen and paper. This whole block should be equivalent to:
            #    for j in range(num_dets_to_consider):
            #        img_gpu = img_gpu * inv_alph_masks[j] + masks_color[j]
            masks_color_summand = masks_color[0]
            if num_dets_to_consider > 1:
                inv_alph_cumul = inv_alph_masks[:(num_dets_to_consider - 1)].cumprod(dim=0)
                masks_color_cumul = masks_color[1:] * inv_alph_cumul
                masks_color_summand += masks_color_cumul.sum(dim=0)

            # img_gpu = img_gpu * inv_alph_masks.prod(dim=0) + masks_color_summand
            img_gpu = inv_alph_masks.prod(dim=0) + masks_color_summand

        # Then draw the stuff that needs to be done on the cpu
        # Note, make sure this is a uint8 tensor or opencv will not anti alias text for whatever reason
        img_numpy = (img_gpu * 255).byte().cpu().numpy()

        if num_dets_to_consider == 0:
            return np.full(img_numpy.shape, 0)

        img_numpy = np.invert(img_numpy)
        # if len(tmp_l) == 0:
        #    return np.full(img_numpy.shape,0)
        return img_numpy



width, height = args.resolution

cam = Camera(width=width, height=height)
dsp = Displayer('MattingV2', cam.width, cam.height, show_info=(not args.hide_fps))
dsp_bg = Displayer('MattingV2_bg', cam.width, cam.height, show_info=(not args.hide_fps))
dsp_ma = Displayer('MattingV2_matting', cam.width, cam.height, show_info=(not args.hide_fps))

model_type=args.model_type
model_backbone =args.model_backbone
model_backbone_scale =args.model_backbone_scale
model_refine_mode =args.model_refine_mode
model_refine_sample_pixels =args.model_refine_sample_pixels
model_refine_threshold =args.model_refine_threshold
#yolactnet = YolactNet(cam =cam)
modnet = MODNetMatte(cam=cam)
'''
bgmv2 = BGMv2(cam =cam, segmodel = yolactnet, model_type=model_type, model_backbone = model_backbone, model_backbone_scale = model_backbone_scale,
              model_refine_mode =model_refine_mode, model_refine_sample_pixels =model_refine_sample_pixels,
              model_refine_threshold = model_refine_threshold)
'''
bgmv2 = BGMv2(cam =cam, segmodel = modnet, model_type=model_type, model_backbone = model_backbone, model_backbone_scale = model_backbone_scale,
              model_refine_mode =model_refine_mode, model_refine_sample_pixels =model_refine_sample_pixels,
              model_refine_threshold = model_refine_threshold)

while True:  # grab bgr
    frame = cam.read()
    key = dsp.step(frame,path = "m_in_256/")
    res = bgmv2.read()
    #bg = yolactnet.read()
    bg = modnet.read()
    if key == ord('q'):
        exit()
    if res is not None:
        key = dsp_ma.step(res,path = "m_res_256/")
    else:
        print("res is None")
    if bg is not None:
        key = dsp_bg.step(bg,path = "m_bg_256/")
    


