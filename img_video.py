import cv2
import numpy as np
import os
from os.path import isfile, join

pathIn = './sg_/'
#pathIn_ = './sg_/'
pathOut = 'video.avi'
fps = 30
frame_array = []
files = [f for f in os.listdir(pathIn) if isfile(join(pathIn, f))]
# for sorting the file names properly
files.sort(key=lambda x: x[5:-4])
files.sort()



frame_array = []
#files = [f for f in os.listdir(pathIn) if isfile(join(pathIn, f))]
# for sorting the file names properly
#files.sort(key=lambda x: x[5:-4])
for i in range(len(files)):
    filename = pathIn + files[i]
    # reading each files
    img = cv2.imread(filename)
    height, width, layers = img.shape
    size = (width, height)
    # names = files[i].split('t')
    # number = int(names[2].split('.')[0])
    # new_file_name = 'out' + format(number, '04') + '.jpg'
    # filename = pathIn_ + new_file_name
    # print(filename)
    # cv2.imwrite(filename, img)


    # inserting the frames into an image array
    frame_array.append(img)
out = cv2.VideoWriter(pathOut, cv2.VideoWriter_fourcc(*'DIVX'), fps, size)
for i in range(len(frame_array)):
    # writing to a image array
    out.write(frame_array[i])
out.release()