import cv2
import numpy as np
import argparse

if __name__ == '__main__':
    # ap = argparse.ArgumentParser()
    # ap.add_argument("-v", "--video", required=True, help="path of video file")
    # ap.add_argument("-o", "--output", required=False, help="Name of output file",default="output")
    # args = vars(ap.parse_args())
    cap = cv2.VideoCapture('vid/1.mp4')
    ret, frame = cap.read()
    #AllFrames = np.zeros(frame.shape, dtype=int)
    frameNUmber = 1


    while True:
        # Capture frame-by-frame
        ret, frame = cap.read()
        if frameNUmber == 1:
            avg = np.float32(frame)

        cv2.accumulateWeighted(frame, avg, 0.005)
        result = cv2.convertScaleAbs(avg)

        #if key == ord("s"):  # it saves and shows image
        cv2.imwrite('im_/output' +format(frameNUmber, '03')+ ".jpg", frame)
        cv2.imwrite('bg_/output' +format(frameNUmber, '03')+ ".jpg", result)
        #cv2.imshow("Result",cv2.resize(AllFrames, dsize=(0, 0), fx=0.2, fy=0.2, interpolation=cv2.INTER_LINEAR))
        #cv2.imshow("Result1 ",cv2.resize(result, dsize=(0, 0), fx=0.2, fy=0.2, interpolation=cv2.INTER_LINEAR) )
        frameNUmber += 1

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()
